FROM openjdk:11.0.4-jdk

COPY target/smallKubeApp*.jar /smallKubeApp.jar

EXPOSE 12001

CMD java -jar /smallKubeApp.jar
