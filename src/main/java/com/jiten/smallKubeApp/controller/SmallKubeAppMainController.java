package com.jiten.smallKubeApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SmallKubeAppMainController {

	@GetMapping("/message")
	public String message() {
		System.out.println("Inside message() Method ======================= ");
		return "Hello from smallKubeApp";
	}
	
}
