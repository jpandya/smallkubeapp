package com.jiten.smallKubeApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmallKubeAppApplication {

	public static void main(String[] args) {
		System.out.println("r̥============= SmallKube Starting ==================================================");
		
		SpringApplication.run(SmallKubeAppApplication.class, args);
	}

}
